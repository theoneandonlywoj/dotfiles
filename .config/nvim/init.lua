-------------------------------------------------
-- WOJCIECH'S NEOVIM CONFIGURATION
-- Neovim website: https://neovim.io/
-- Wojciech's dotfiles: https://gitlab.com/theoneandonlywoj/dotfiles
-------------------------------------------------

require("theoneandonlywoj/settings")
require("theoneandonlywoj/plugins")
