-- Plugins
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
    ---------------------
    -- Package Manager --
    ---------------------

    use 'wbthomason/packer.nvim'
    
    -------------
    -- Lualine --
    -------------

    use ({
        'nvim-lualine/lualine.nvim',
        config = function()
            require('theoneandonlywoj/plugins/lualine')
        end
    })

    -------------------------
    -- Nvim-Base 16 Themes --
    -------------------------

    use("RRethy/nvim-base16")
    vim.cmd('colorscheme base16-gruvbox-dark-soft')

    -- Packer automatic sync if the plugins.lua file have been changed.
    vim.cmd([[
        augroup packer_user_config
        autocmd!
        autocmd BufWritePost plugins.lua source <afile> | PackerSync
        augroup END
    ]])

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
